
DESCRIPTION
----------------
UEditor is Baidu web front-end R & D department developed
WYSIWYG rich text web editor, with a lightweight, customizable,
and focus on user experience and other characteristics,
the open source BSD license, allowing free use and modify the code.

Installation
-------------
1. Download the ueditor 1.20 in
http://ueditor.baidu.com/build/build_down.php?t=1_2_3_0-utf8-php.
2. Unzip it into sites/all/libraries,
so that there's like sites/all/libraries/ueditor/editor_all_min.js.
3. Enabled ueditor module.

Configuration
-----------------
This file is chinese language by default.
You can custom configuration in
http://ueditor.baidu.com/website/ipanel/panel.html,

Because the editor official website provides
very complete configuration tool.
So, There will be no more configuration options appear
in the WYSIWYG module configuration page.
Just put in the official website of the generated files
copy into the sites/all/libraries, and write the file name
in the WYSIWYG module configuration page textfield(Config js filename).

If you want, you can use the website to provide advanced configuration page.
It is in the second tab.

Unfortunately, the official website of the Chinese,
You can use the google translation, the operation so easy.
