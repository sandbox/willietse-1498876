(function($) {

/**
 * Attach this editor to a target element.
 */
Drupal.wysiwyg.editor.attach.ueditor = function (context, params, settings) {
  var editorOption = {
    lang: (settings['language'] ? settings['language'] : 'en'),
    zIndex: (settings['zindex'] ? settings['zindex'] : 99),
    initialContent: (settings['initialContent'] ? settings['initialContent'] : ''),
  };
  var editor = new baidu.editor.ui.Editor(editorOption);
  // Attach editor.
  editor.render(params.field);

  Drupal.wysiwyg.instances[params.field]['rendered'] = editor;
};

/**
 * Detach a single or all editors.
 */
Drupal.wysiwyg.editor.detach.ueditor = function (context, params, trigger) {
  // Get editor.
  var editor = Drupal.wysiwyg.instances[params.field]['rendered'];
  // Move content to original textarea.
  $('textarea#' + params.field).val(editor.getContent());
  // Show for original textarea.
  $('textarea#' + params.field).show();
  // Remove and destroy editor.
  $('div#' + params.field).remove();
  editor.destroy();
};

})(jQuery);
