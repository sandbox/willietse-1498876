<?php

/**
 * @file
 * Editor integration functions for ueditor.
 */

/**
 * Plugin implementation of hook_editor().
 */
function ueditor_ueditor_editor() {
  $editor['ueditor'] = array(
    'title' => 'Ueditor',
    'vendor url' => 'http://ueditor.baidu.com',
    'download url' => 'http://ueditor.baidu.com/build/build_down.php?t=1_2_3_0-utf8-php',
    'libraries' => array(
      '' => array(
        'title' => 'Minified',
        'files' => array('editor_all_min.js'),
      ),
      'src' => array(
        'title' => 'Source',
        'files' => array('editor_all.js'),
      ),
    ),
    'version callback' => 'ueditor_version',
    'settings callback' => 'ueditor_settings',
    'settings form callback' => 'ueditor_settings_form',
    'versions' => array(
      '1.2.3.0' => array(
        'js files' => array('ueditor.js'),
      ),
    ),
  );
  return $editor;
}

/**
 * Detect editor version.
 *
 * @param array $editor
 *   An array containing editor properties as returned from hook_editor().
 *
 * @return int
 *   The installed editor version.
 */
function ueditor_version($editor) {
  $library = $editor['library path'] . '/editor_all.js';
  if (!file_exists($library)) {
    return;
  }
  $library = fopen($library, 'r');
  $max_lines = 100;
  while ($max_lines && $line = fgets($library, 500)) {
    // version:'CKEditor 3.0 SVN',revision:'3665'
    // version:'3.0 RC',revision:'3753'
    // version:'3.0.1',revision:'4391'
    if (preg_match('/UE.version = \"(.*)\";/', $line, $version)) {
      fclose($library);
      return $version[1];
    }
    $max_lines--;
  }
  fclose($library);
}

/**
 * Enhances the editor profile settings form for CKEditor.
 */
function ueditor_settings_form(&$form, &$form_state) {
  // Customize config js filename.
  $form['basic']['config'] = array(
    '#type' => 'textfield',
    '#title' => t('Editor config js filename'),
    '#description' => t('1, Customize your config in http://ueditor.baidu.com/website/ipanel/panel.html.<br />
      2, Put it into sites/all/libraries(like: sites/all/libraries/ueditor/editor_config.js).<br />'),
    '#default_value' => isset($form_state['wysiwyg_profile']->settings['config']) ? $form_state['wysiwyg_profile']->settings['config'] : 'editor_config.js',
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['basic']['zindex'] = array(
    '#type' => 'textfield',
    '#title' => t('Editor zindex'),
    '#description' => t('The official website of the default zindex 900,<br />
      and Drupal overlay module ( #overlay= page ) conflict,
      so default change from 900 to 90 or you can customize.'),
    '#default_value' => isset($form_state['wysiwyg_profile']->settings['zindex']) ? $form_state['wysiwyg_profile']->settings['zindex'] : 99,
    '#size' => 5,
    '#maxlength' => 4,
    '#required' => TRUE,
  );
  $form['basic']['initial_content'] = array(
    '#type' => 'textfield',
    '#title' => t('Editor initial content'),
    '#description' => t('Editor initial content, after editor loading in the textarea.'),
    '#default_value' => isset($form_state['wysiwyg_profile']->settings['initial_content']) ? $form_state['wysiwyg_profile']->settings['initial_content'] : '',
    '#maxlength' => 255,
  );
}

/**
 * Return runtime editor settings for a given wysiwyg profile.
 *
 * @param array $editor
 *   A processed hook_editor() array of editor properties.
 * @param array $config
 *   An array containing wysiwyg editor profile settings.
 * @param string $theme
 *   The name of a theme/GUI/skin to use.
 *
 * @return array
 *   A settings array to be populated in
 *   Drupal.settings.wysiwyg.configs.{editor}
 */
function ueditor_settings($editor, $config, $theme) {
  // Settings.
  $settings['editorPath'] = base_path() . $editor['library path'] . '/';
  $settings['language'] = (!empty($config['language']) ? ueditor_wysiwyg_language_trans($config['language']) : 'en');
  $settings['zindex'] = (!empty($config['zindex']) ? $config['zindex'] : 99);
  $settings['initialContent'] = (!empty($config['initial_content']) ? $config['initial_content'] : '');

  // Load config js.
  drupal_add_js('window.UEDITOR_HOME_URL = "' . $settings['editorPath'] . '";', array('type' => 'inline', 'weight' => -2));
  drupal_add_js($editor['library path'] . '/' . (!empty($config['config']) ? $config['config'] : 'editor_config.js'), array('weight' => -1));

  // Theme.
  // Ueditor temporarily does not support multiple themes,
  // Can customize CSS overwrite.
  drupal_add_css($editor['library path'] . '/themes/default/ueditor.css');
  drupal_add_css($editor['library path'] . '/themes/default/iframe.css');

  return $settings;
}
